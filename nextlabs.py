c = {"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},
               {"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},
               {"id":650},{"id":651},{"id":652},{"id":653}],
     "errors":[{"code":3,"message":"[PHP Warning #2]count(): Parameter must be an array or an object that implements Countable (153)"}]}

m = []
for i in c.values():
    for j in i:
        for k in j.values():
            if type(k) == int:
                m.append(k)

print(m)
 '''  TASK 2,3  sir to be frank i understand this code but i dont know about web scrapping with this tasks i am happy to learn new thing webscraping with nthis tasks i installed
so many packegs lxml,scrapy,beautifulsoup4,selenium '''

from bs4 import BeautifulSoup
import requests, lxml, re, json
from datetime import datetime


headers = {
    "user-agent":
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36"
}

params = {
    "id": " Indian studio Gametion Technologies Pvt Ltd",  # app name
    "gl": "INDIA"                  # country
}


def scrape_google_store_app():
    html = requests.get("https://play.google.com/store/apps/details?id=com.ludo.king&gl=in&hl=en", params=params, headers=headers, timeout=10).text
    soup = BeautifulSoup(html, "lxml")

    app_data = []


    basic_app_info = json.loads(re.findall(r"<script nonce=\".*\" type=\"application/ld\+json\">(.*?)</script>",
                                           str(soup.select("script")[12]), re.DOTALL)[0])

    app_name = basic_app_info["name"]
    app_type = basic_app_info["@type"]
    app_url = basic_app_info["url"]
    app_category = basic_app_info["applicationCategory"]

    app_content_rating = basic_app_info["contentRating"]
    app_rating = round(float(basic_app_info["aggregateRating"]["ratingValue"]), 1)  # 4.287856 -> 4.3
    app_reviews = basic_app_info["aggregateRating"]["ratingCount"]

    app_author = basic_app_info["author"]["name"]
    app_author_url = basic_app_info["author"]["url"]

    app_user_comments = []

    app_user_reviews_data = re.findall(r"(\[\"gp.*?);</script>",
                                       str(soup.select("script")), re.DOTALL)

    for review in app_user_reviews_data:
        user_name = re.findall(r"\"gp:.*?\",\s?\[\"(.*?)\",", str(review))

        user_avatar = [avatar.replace('"', "") for avatar in re.findall(r"\"gp:.*?\"(https.*?\")", str(review))]

        user_comments = [comment.replace('"', "").replace("'", "") for comment in
                        re.findall(r"gp:.*?https:.*?]]],\s?\d+?,.*?,\s?(.*?),\s?\[\d+,", str(review))]

        user_comment_app_rating = re.findall(r"\"gp.*?https.*?\],(.*?)?,", str(review))

        for name, avatar, comment,  user_app_rating in zip(user_name,user_avatar,user_comments,user_comment_app_rating):
        
            app_user_comments.append({
                "user_name": name,
                "user_avatar": avatar,
                "comment": comment,
                "user_app_rating": user_app_rating,
            })

        app_data.append({
            "app_name": app_name,
            "app_type": app_type,
            "app_url": app_url,
            "app_main_thumbnail": app_main_thumbnail,
            "app_description": app_description,
            "app_content_rating": app_content_rating,
            "app_category": app_category,
            "app_operating_system": app_operating_system,
            "app_rating": app_rating,
            "app_reviews": app_reviews,
            "app_author": app_author,
            "app_author_url": app_author_url,
            "app_screenshots": app_images
        })

        return {"app_data": app_data, "app_user_comments": app_user_comments}


print(json.dumps(scrape_google_store_app(), indent=2))

